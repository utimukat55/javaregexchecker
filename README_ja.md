![Java Regex Checker](./JRegexChecker.png)

# JavaRegexChecker

(Javaの)正規表現のチェックとどのように正規表現が適用されるのかを簡単に可視化するJavaアプリです。

# 必要なもの

- Java8以降 (0.1.2にてJava8でも実行できるように変更)
- Mavenを使用可能なPC (実行可能jarファイルをビルドする際に必要です)

# 実行イメージ

![](./demo.png)

# 使用方法

## Mavenでのビルド

実行可能jarファイルをビルドする場合は、以下のコマンドを実行して下さい。

```
$ git clone https://gitlab.com/utimukat55/javaregexchecker.git
$ cd javaregexchecker/JavaRegexChecker
$ mvn package
```

コマンドを実行すると、`target`ディレクトリに `JavaRegexChecker-0.1.2-uber-jar.jar` (実行可能jarファイル)が生成されます。

## JavaRegexCheckerの実行

以下のコマンドで起動します。

```
$ java -jar target/JavaRegexChecker-0.1.2-uber-jar.jar 
```

![JavaRegexChecker](blank.png)

1. `Target`テキストエリアに正規表現を適用させる文字列を入力します
2. `Regex String`テキストエリアに正規表現文字列を入力します
3. (置換後の文字列を使用する場合) `Replace String`に置換文字列を入力します
4. `java.lang.String#split() result` と `java.lang.String#replaceAll() result`に適用した文字列が表示されます

![Sample](./JRegexChecker.png)

特記事項: `Target`と`Regex String`と`Replace String`は`javax.swing.event.DocumentListener`を設定しているため、適用結果の2つのテキストエリアは入力文字列の変更をリアルタイムに反映します。

### java.util.regex.Pattern#compile(String, int)の flags を指定する(0.1.0〜)

コンパイルフラグを設定する場合は、メニューからOption→Pattern#compile flagsと辿って適用するフラグを選択して下さい。既にターゲット文字列が入力済みの場合は、設定を変更すると即時に変更を適用します。

![java.util.regex.Pattern#compile flag](./flags.png)

# ライセンス

JavaRegexCheckerはCC0で提供されます。 ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed))

# 使用ライブラリ

JavaRegexCheckerは以下のライブラリを使用しています。

- JTextComponent-Popup (https://gitlab.com/utimukat55/jtextcomponent-popup)
- JAboutDialog (https://gitlab.com/utimukat55/jaboutdialog)

# その他サポート記事, バイナリなど

https://kasu-kasu.ga/tags/javaregexchecker/

