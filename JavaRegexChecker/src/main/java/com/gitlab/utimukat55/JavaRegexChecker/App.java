/**
 * Java Regex Checker
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.JavaRegexChecker;

/**
 * Entry point.
 */
public class App {
	public static void main(String[] args) {
		MainFrame.main(args);
	}
}
