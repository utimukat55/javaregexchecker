/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.inner;

public class Constant {

	private Constant() {}
	public static final String SOFTWARENAME = "SoftwareName";
	public static final String SOFTWAREVERSION = "SoftwareVersion";
	public static final String SOFTWAREURL = "SoftwareUrl";
	public static final String SOFTWARELICENSE = "SoftwareLicense";
	public static final String SOFTWARECOPYRIGHT = "SoftwareCopyright";
	public static final String SOFTWAREICON = "SoftwareIcon";
	public static final String DEPENDLIBRARY_PREFIX = "DependLibrary";
	public static final String DEPENDLIBRARYNAME_SUFFIX = ".name";
	public static final String DEPENDLIBRARYVERSION_SUFFIX = ".version";
	public static final String DEPENDLIBRARYLICENSE_SUFFIX = ".license";
	public static final String DEPENDLIBRARYURL_SUFFIX = ".url";
	public static final String DEPENDLIBRARYLICENSEPATH_SUFFIX = ".licensepath";
	public static final String DEPENDLIBRARYCOPYRIGHTOWNER_SUFFIX = ".copyrightowner";
	public static final String CONTRIBUTOR_PREFIX = "Contributor";
	public static final String CONTRIBUTORNAME_SUFFIX = ".name";
	public static final String CONTRIBUTORURL_SUFFIX = ".url";
}