/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.jtextcomponent_popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.text.Document;

import com.gitlab.utimukat55.jtextcomponent_popup.internal.JPopupConstants;
import com.gitlab.utimukat55.jtextcomponent_popup.internal.JPopupUtil;

/**
 * JTextField with cut/copy/paste functions.
 * 
 * @see javax.swing.JTextField
 * @author utimukat55
 */
public class JPopupTextField extends JTextField implements ActionListener {

	private static final long serialVersionUID = -7877331551702093325L;
	private JMenuItem cut;
	private JMenuItem copy;
	private JMenuItem paste;
	private JPopupMenu menu;

	private String LABEL_CUT;
	private String LABEL_COPY;
	private String LABEL_PASTE;

	/**
	 * {@inheritDoc}
	 */
	public JPopupTextField() {
		super();
		setupMenuLabel();
		setupPopupMenu();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public JPopupTextField(String text) {
		super(text);
		setupMenuLabel();
		setupPopupMenu();
	}

	/**
	 * {@inheritDoc}
	 */
	public JPopupTextField(int columns) {
        super(columns);
		setupMenuLabel();
		setupPopupMenu();
    }
	
	/**
	 * {@inheritDoc}
	 */
    public JPopupTextField(String text, int columns) {
        super(text, columns);
		setupMenuLabel();
		setupPopupMenu();
    }

	/**
	 * {@inheritDoc}
	 */
    public JPopupTextField(Document doc, String text, int columns) {
       super(doc, text, columns);
		setupMenuLabel();
		setupPopupMenu();
    }

	private void setupMenuLabel() {
		LABEL_CUT = JPopupUtil.getCutLabelByLocale();
		LABEL_COPY = JPopupUtil.getCopyLabelByLocale();
		LABEL_PASTE = JPopupUtil.getPasteLabelByLocale();
	}

	private void setupPopupMenu() {
		cut = new JMenuItem(LABEL_CUT);
		cut.setMnemonic(JPopupConstants.NEMONIC_CUT);
		cut.addActionListener(this);

		copy = new JMenuItem(LABEL_COPY);
		copy.setMnemonic(JPopupConstants.NEMONIC_COPY);
		copy.addActionListener(this);

		paste = new JMenuItem(LABEL_PASTE);
		paste.setMnemonic(JPopupConstants.NEMONIC_PASTE);
		paste.addActionListener(this);

		menu = new JPopupMenu();
		menu.add(cut);
		// menu.addSeparator(); // uncomment if seprator needs between copy and paste.
		menu.add(copy);
		menu.add(paste);
		setComponentPopupMenu(menu);		
	}
	
	/**
	 * Function definition.
	 * @param e event where fired.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cut) {
			// cut to clipboard.
			cut();
		} else if (e.getSource() == copy) {
			// copy to clipboard.
			copy();
		} else if (e.getSource() == paste) {
			// paste to TextField.
			paste();
		}
	}
}
