/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.jtextcomponent_popup.internal;

import java.util.Locale;

/**
 * Utility class for JTextComponent-popup.
 * @author utimukat55
 *
 */
public class JPopupUtil {

	private JPopupUtil() {
	}
	
	/**
	 * Get copy label by current locale.
	 * @return copy label by current locale
	 */
	public static String getCopyLabelByLocale() {
		String lang = Locale.getDefault().getLanguage();
		
		if (lang.equals(Locale.ENGLISH.getLanguage())) {
			return JPopupConstants.LABEL_COPY;
		} else if (lang.equals(Locale.JAPANESE.getLanguage())) {
			return JPopupConstants.LABEL_COPY_JA;
		} else {
			return JPopupConstants.LABEL_COPY;
		}
	}

	/**
	 * Get cut label by current locale.
	 * @return cut label by current locale
	 */
	public static String getCutLabelByLocale() {
		String lang = Locale.getDefault().getLanguage();
		
		if (lang.equals(Locale.ENGLISH.getLanguage())) {
			return JPopupConstants.LABEL_CUT;
		} else if (lang.equals(Locale.JAPANESE.getLanguage())) {
			return JPopupConstants.LABEL_CUT_JA;
		} else {
			return JPopupConstants.LABEL_CUT;
		}
	}

	/**
	 * Get paste label by current locale.
	 * @return paste label by current locale
	 */
	public static String getPasteLabelByLocale() {
		String lang = Locale.getDefault().getLanguage();
		
		if (lang.equals(Locale.ENGLISH.getLanguage())) {
			return JPopupConstants.LABEL_PASTE;
		} else if (lang.equals(Locale.JAPANESE.getLanguage())) {
			return JPopupConstants.LABEL_PASTE_JA;
		} else {
			return JPopupConstants.LABEL_PASTE;
		}
	}
}
