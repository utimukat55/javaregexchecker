/**
 * Java Regex Checker
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
module com.gitlab.utimukat55.JavaRegexChecker {
	requires java.desktop;
	requires java.logging;
}