/**
 * Java Regex Checker
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.JavaRegexChecker;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.gitlab.utimukat55.jaboutdialog.JAboutDialog;
import com.gitlab.utimukat55.jtextcomponent_popup.JPopupTextArea;
import com.gitlab.utimukat55.jtextcomponent_popup.JPopupTextField;

public class MainFrame extends JFrame implements DocumentListener {

	private static final long serialVersionUID = 8910429879692356346L;
	private static final Logger logger = Logger.getLogger(MainFrame.class.getName());

	private JPanel contentPane;
	private JTextField regexTextField;
	private JTextArea targetTextArea;
	private JTextArea splitResultTextArea;

	private JTextField replaceTextField;
	private JTextArea replaceResultTextArea;

	private Frame f = this;
	private Component c = f;
	private int patternCompileFlg = 0;
	
	private JCheckBoxMenuItem chkCaseInsensitive;
	private JCheckBoxMenuItem chkMultiline;
	private JCheckBoxMenuItem chkDotall;
	private JCheckBoxMenuItem chkUnicodeCase;
	private JCheckBoxMenuItem chkCanonEq;
	private JCheckBoxMenuItem chkUnixLines;
	private JCheckBoxMenuItem chkLiteral;
	private JCheckBoxMenuItem chkUnicodeCharacterClass;
	private JCheckBoxMenuItem chkComments;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Util.improbeLogFormat();
		Util.setConsoleLogAll();
		logger.setLevel(Level.CONFIG);
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		EventQueue.invokeLater(() -> {
			logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
			try {
				MainFrame frame = new MainFrame();
				frame.setVisible(true);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Exception in run()", e);
			}
			logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		});
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setTitle("Java Regex Checker");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 408, 437);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menuOption = new JMenu("Option");
		menuOption.setMnemonic(KeyEvent.VK_O);
		menuBar.add(menuOption);
		
		JMenu mnNewMenu = new JMenu("Pattern#compile() flags");
		menuOption.add(mnNewMenu);
		
		chkCaseInsensitive = new JCheckBoxMenuItem("CASE_INSENSITIVE(?i)");
		mnNewMenu.add(chkCaseInsensitive);
		
		chkMultiline = new JCheckBoxMenuItem("MULTILINE(?m)");
		mnNewMenu.add(chkMultiline);
		
		chkDotall = new JCheckBoxMenuItem("DOTALL(?s)");
		mnNewMenu.add(chkDotall);
		
		chkUnicodeCase = new JCheckBoxMenuItem("UNICODE_CASE(?u)");
		mnNewMenu.add(chkUnicodeCase);
		
		chkCanonEq = new JCheckBoxMenuItem("CANON_EQ");
		mnNewMenu.add(chkCanonEq);
		
		chkUnixLines = new JCheckBoxMenuItem("UNIX_LINES(?d)");
		mnNewMenu.add(chkUnixLines);
		
		chkLiteral = new JCheckBoxMenuItem("LITERAL");
		mnNewMenu.add(chkLiteral);
		
		chkUnicodeCharacterClass = new JCheckBoxMenuItem("UNICODE_CHARACTER_CLASS(?U)");
		mnNewMenu.add(chkUnicodeCharacterClass);
		
		chkComments = new JCheckBoxMenuItem("COMMENTS(?x)");
		mnNewMenu.add(chkComments);
		
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_H);
		menuBar.add(menuHelp);
		
		JMenuItem menuItemAbout = new JMenuItem("About");
		menuItemAbout.setMnemonic(KeyEvent.VK_A);
		menuItemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
				
				JAboutDialog dialog = new JAboutDialog(f, true);
				Properties prop = new Properties();
				try (
						InputStream is = getClass().getResourceAsStream("/jaboutdialog.xml")
				) {
					prop.loadFromXML(is);
					dialog.setInfo(prop);
					dialog.setLocationRelativeTo(c);
					dialog.setVisible(true);
				} catch (IOException e) {
					logger.log(Level.SEVERE, "failed to create JAboutDialog", e);
				}
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
			}
		});
		menuHelp.add(menuItemAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JScrollPane targetPane = new JScrollPane();
		targetPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JLabel lblTarget = new JLabel("Target");

		JLabel lblRegexString = new JLabel("Regex String");

		regexTextField = new JPopupTextField();
		regexTextField.setColumns(10);

		JLabel lblJavalangstringsplitResult = new JLabel("java.lang.String#split() result");

		JScrollPane splitResultPane = new JScrollPane();
		splitResultPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		JLabel lblReplaceString = new JLabel("Replace String");
		
		replaceTextField = new JPopupTextField();
		replaceTextField.setColumns(10);
		
		JLabel lblJavalangstringreplaceallResult = new JLabel("java.lang.String#replaceAll() result");
		
		JScrollPane replaceResultPane = new JScrollPane();
		replaceResultPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(replaceResultPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
						.addComponent(splitResultPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
						.addComponent(targetPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
						.addComponent(lblTarget, Alignment.LEADING)
						.addComponent(lblJavalangstringsplitResult, Alignment.LEADING)
						.addComponent(lblJavalangstringreplaceallResult, Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
									.addComponent(regexTextField, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblRegexString)
									.addGap(86)))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblReplaceString)
								.addComponent(replaceTextField, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblTarget)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(targetPane, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblRegexString)
						.addComponent(lblReplaceString))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(regexTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(replaceTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblJavalangstringsplitResult)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(splitResultPane, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblJavalangstringreplaceallResult)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(replaceResultPane, GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		replaceResultTextArea = new JPopupTextArea();
		replaceResultTextArea.setLineWrap(true);
		replaceResultPane.setViewportView(replaceResultTextArea);
		
		splitResultTextArea = new JPopupTextArea();
		splitResultPane.setViewportView(splitResultTextArea);

		targetTextArea = new JPopupTextArea();
		targetTextArea.setLineWrap(true);
		targetPane.setViewportView(targetTextArea);
		contentPane.setLayout(gl_contentPane);

		addListeners();
	}
	
	private void addListeners() {
		// add ItemListener to apply Pattern#compile() flag change
		chkCaseInsensitive.addItemListener(e  -> {
			setPatternCompileFlg();
			update();			
		});
		chkMultiline.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});
		chkDotall.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});
		chkUnicodeCase.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});
		chkCanonEq.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});
		chkUnixLines.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});
		chkLiteral.addItemListener(e ->  {
			setPatternCompileFlg();
			update();
		});
		chkUnicodeCharacterClass.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});
		chkComments.addItemListener(e -> {
			setPatternCompileFlg();
			update();
		});

		// add DocumentListener to realize value changing.
		targetTextArea.getDocument().addDocumentListener(this);
		regexTextField.getDocument().addDocumentListener(this);
		replaceTextField.getDocument().addDocumentListener(this);
	}

	private void setPatternCompileFlg() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		int updateFlg = 0;
		if (chkCaseInsensitive.isSelected()) {
			updateFlg = updateFlg | Pattern.CASE_INSENSITIVE;
		}
		if (chkMultiline.isSelected()) {
			updateFlg = updateFlg | Pattern.MULTILINE;
		}
		if (chkDotall.isSelected()) {
			updateFlg = updateFlg | Pattern.DOTALL;
		}
		if (chkUnicodeCase.isSelected()) {
			updateFlg = updateFlg | Pattern.UNICODE_CASE;
		}
		if (chkCanonEq.isSelected()) {
			updateFlg = updateFlg | Pattern.CANON_EQ;
		}
		if (chkUnixLines.isSelected()) {
			updateFlg = updateFlg | Pattern.UNIX_LINES;
		}
		if (chkLiteral.isSelected()) {
			updateFlg = updateFlg | Pattern.LITERAL;
		}
		if (chkUnicodeCharacterClass.isSelected()) {
			updateFlg = updateFlg | Pattern.UNICODE_CHARACTER_CLASS;
		}
		if (chkComments.isSelected()) {
			updateFlg = updateFlg | Pattern.COMMENTS;
		}
		
		patternCompileFlg = updateFlg;
		logger.info("updatedFlg:[" + patternCompileFlg + "]");

		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		update(arg0);
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		update(arg0);
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		update(arg0);
	}

	private void update(DocumentEvent de) {
		// not to use DocumentEvent
		update();
	}
	
	private void update() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		try {
			Pattern p = Pattern.compile(regexTextField.getText(), patternCompileFlg);

			splitResultTextArea.setText(String.join("\n", p.split(targetTextArea.getText())));
			replaceResultTextArea.setText(p.matcher(targetTextArea.getText()).replaceAll(replaceTextField.getText()));
			
		} catch (PatternSyntaxException e) {
			logger.log(Level.WARNING, "Couldn't recognize as regex string:[" + regexTextField.getText() + "]", e);
		}
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}
}
