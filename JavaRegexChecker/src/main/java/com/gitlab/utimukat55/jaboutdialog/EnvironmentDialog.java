/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.gitlab.utimukat55.jaboutdialog.inner.Util;

public class EnvironmentDialog extends JDialog {

	
	private static final long serialVersionUID = -6740875916089625979L;
	private static final Logger logger = Logger.getLogger(EnvironmentDialog.class.getName());

	private final JPanel contentPanel = new JPanel();
	private JTable tableSystemProperty;
	private JTable tableEnvironmentVariable;

	private String environmentString = "";
	
	private TreeMap<String, String> sortedSystemProperty = null;
	private TreeMap<String, String> environmentVariable = null;
	private final String[] columnLabel = {"Key", "Value"};
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		try {
			EnvironmentDialog dialog = new EnvironmentDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	/**
	 * Create the dialog.
	 */
	public EnvironmentDialog() {
		super();
		createLayout();
	}

	public EnvironmentDialog(Dialog owner, boolean modal) {
		super(owner, modal);
		createLayout();
	}

	private void createLayout() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");

		nonGuiProcess();

		setTitle("System Properties / Environment Variables");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());

		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel lblSystemProperty = new JLabel("System Property:");
		JScrollPane scrollSystemProperty = new JScrollPane();
		JLabel lblEnvironmentVariable = new JLabel("Environment Variable:");
		JScrollPane scrollEnvironmentVariable = new JScrollPane();
		JButton btnNewButton = new JButton("Copy all values to clipboard");
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);

		tableEnvironmentVariable = new JTable(Util.convertMapToStrings(environmentVariable), columnLabel);
		tableSystemProperty = new JTable(Util.convertMapToStrings(sortedSystemProperty), columnLabel);
		scrollSystemProperty.setViewportView(tableSystemProperty);

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
				Clipboard clipboard = Toolkit.getDefaultToolkit()
			            .getSystemClipboard();
			    StringSelection selection = new StringSelection(environmentString);
			    clipboard.setContents(selection, selection);
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
			}
		});

		tableEnvironmentVariable.setEnabled(false);
		scrollEnvironmentVariable.setViewportView(tableEnvironmentVariable);
		
		tableSystemProperty.setEnabled(false);
		
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollSystemProperty, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblSystemProperty)
							.addPreferredGap(ComponentPlacement.RELATED, 224, Short.MAX_VALUE)
							.addComponent(btnNewButton))
						.addComponent(lblEnvironmentVariable)
						.addComponent(scrollEnvironmentVariable, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSystemProperty)
						.addComponent(btnNewButton))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollSystemProperty, GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEnvironmentVariable)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollEnvironmentVariable, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
					.addGap(14))
		);

		contentPanel.setLayout(gl_contentPanel);

		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");

	}

	private void nonGuiProcess() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		sortedSystemProperty = Util.createSystemProperty();
		environmentVariable = Util.createEnvironmentVariable();
		environmentString = createEnvironmentString();
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	private String createEnvironmentString() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		// preparing for copy to clipboard
		StringBuilder sb = new StringBuilder();
		sb.append("System Property:\n");
		for(Map.Entry<String, String> entry : sortedSystemProperty.entrySet()) {
			sb.append("Key:[" + entry.getKey() + "]/Value:[" + entry.getValue() + "]\n");
        }
		sb.append("\n");
		sb.append("Environment Variable:\n");
		for(Map.Entry<String, String> entry : environmentVariable.entrySet()) {
			sb.append("Key:[" + entry.getKey() + "]/Value:[" + entry.getValue() + "]\n");
        }
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		
		return sb.toString();
	}
}