![Java Regex Checker](./JRegexChecker.png)

# JavaRegexChecker

Java application to check regular expression is correct and make visible how a regular expression is applied works to target String.

# Prerequisites

- Java8+ (0.1.2 expand to Java8)
- Maven installed PC (to build executable jar file)

# Executing Image

![](./demo.png)

# How to use

## Maven build

To build executable jar file, run commands below.

```
$ git clone https://gitlab.com/utimukat55/javaregexchecker.git
$ cd javaregexchecker/JavaRegexChecker
$ mvn package
```

These commands make `JavaRegexChecker-0.1.2-uber-jar.jar` (executable jar file) in `target` directory.

## Run JavaRegexChecker

To try sample app, execute commands below.

```
$ java -jar target/JavaRegexChecker-0.1.2-uber-jar.jar 
```

![JavaRegexChecker](blank.png)

1. input string to `Target` TextArea which is to affect regular expression.
2. input regular expression string to `Regex String`
3. (if want to replace) input replaced string to `Replace String`
4. affected string will be displayed in `java.lang.String#split() result` and `java.lang.String#replaceAll() result`

![Sample](./JRegexChecker.png)

note: `Target`, `Regex String` and `Replace String`has set `javax.swing.event.DocumentListener`. 2 results updated any input string changed.

### Specify flags of java.util.regex.Pattern#compile(String, int) (from 0.1.0)

When specify flags of `java.util.regex.Pattern#compile(String, int)`, please choose the menu Options -> Pattern#compile flags . If already Target string set, the choice will apply immediately.

![java.util.regex.Pattern#compile flag](./flags.png)

# License

JavaRegexChecker is licensed by CC0 ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed)).

# Using libraries

JavaRegexChecker uses these library.

- JTextComponent-Popup (https://gitlab.com/utimukat55/jtextcomponent-popup)
- JAboutDialog (https://gitlab.com/utimukat55/jaboutdialog)

# Other Information (written in Japanese), runnable jar file

https://kasu-kasu.ga/tags/javaregexchecker/

